<?php

namespace App\Controller;

use App\Manager\CalculatorManager;
use App\Service\CalculatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CalculatorController extends AbstractController
{
    public function calculateAction(Request $request, CalculatorManager $manager, CalculatorService $service): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        return new JsonResponse($manager->save($service->resolve($data)), Response::HTTP_OK);
    }
}