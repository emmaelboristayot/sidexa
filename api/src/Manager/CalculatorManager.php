<?php

namespace App\Manager;

use App\Entity\Calculator;
use App\Form\CalculatorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class CalculatorManager
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager)
    {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $data
     * @return int|null
     */
    public function save(array $data): ?int
    {
        $calculator = new Calculator();
        $form = $this->formFactory->create(CalculatorType::class, $calculator);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
//            $this->entityManager->persist($calculator);
//            $this->entityManager->flush();

            return $calculator->getTotal();
        }

        return null;
    }
}