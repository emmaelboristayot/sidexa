<?php

namespace App\Service\Exception;

use Exception;

class AriphmeticException extends Exception
{
    function __construct($msg, $code) {
        return parent::__construct($msg, $code);
    }
    function __toString() {
        return get_class($this) . '('
            . $this->code . '): '
            . $this->message;
    }
}