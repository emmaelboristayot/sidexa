import React, { useState } from 'react';
import './Calculator.css';
import format from "./logic/format";

const Calculator = () => {
  const stateObj = {
    total: null,
    next: null,
    symbol: null,
    operation: "",
  };

  const [properties, setProperties] = useState(stateObj);

  const handleClick = ({target}) => {
    const btnValue = target.textContent;

    const newProperties = { ...properties };
    setProperties(format(newProperties, btnValue));

    if (btnValue === '=') {
      if (properties.next && btnValue) {

        const requestOptions = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ operation: properties.operation })
        };
        fetch('http://127.0.0.1:8000/api/calculate/', requestOptions)
            .then(response => response.json())
            .then(data => {
              console.log(data)
              // setProperties({ total: data.id })
            });
      }
    }
  };

  return (
    <>
      <div className="container">
        <div className="screen">
          {/*{ properties.total}*/}
          {/*{ properties.symbol }*/}
          {/*{ properties.next }*/}
          { properties.operation}
        </div>
        <button type="button" onClick={handleClick} className="btn">AC</button>
        <button type="button" onClick={handleClick} className="btn">(</button>
        <button type="button" onClick={handleClick} className="btn">)</button>
        <button type="button" onClick={handleClick} className="btn orange">÷</button>
        <button type="button" onClick={handleClick} className="btn">7</button>
        <button type="button" onClick={handleClick} className="btn">8</button>
        <button type="button" onClick={handleClick} className="btn">9</button>
        <button type="button" onClick={handleClick} className="btn orange">x</button>
        <button type="button" onClick={handleClick} className="btn">4</button>
        <button type="button" onClick={handleClick} className="btn">5</button>
        <button type="button" onClick={handleClick} className="btn">6</button>
        <button type="button" onClick={handleClick} className="btn orange">-</button>
        <button type="button" onClick={handleClick} className="btn">1</button>
        <button type="button" onClick={handleClick} className="btn">2</button>
        <button type="button" onClick={handleClick} className="btn">3</button>
        <button type="button" onClick={handleClick} className="btn orange">+</button>
        <button type="button" onClick={handleClick} className="btn zero">0</button>
        <button type="button" onClick={handleClick} className="btn">.</button>
        <button type="button" onClick={handleClick} className="btn orange">=</button>
      </div>
    </>
  );
};

export default Calculator;
