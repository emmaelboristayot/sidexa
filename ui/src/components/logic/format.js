
function isNumber(item) {
  return !!item.match(/[0-9]+/);
}

export default function format(obj, buttonName) {
  if (buttonName === 'AC') {
    return {
      total: 0,
      next: null,
      symbol: null,
      operation: null,
    };
  }

  if (isNumber(buttonName)) {
    if (buttonName === '0' && obj.next === '0') {
      return {};
    }

    // If there is no symbol, update next and clear the value
    if (obj.next) {
      return {
        operation: obj.operation + buttonName,
        next: obj.next + buttonName,
        total: null,
      };
    }
    return {
      operation: obj.operation + buttonName,
      next: buttonName,
      total: null,
    };
  }

  if (buttonName === '.') {
    if (obj.next) {
      if (obj.next.includes('.')) {
        return { ...obj };
      }
      return { ...obj, next: `${obj.next}.`, operation: `${obj.operation}.` };
    }
    if (obj.symbol) {
      return { next: '0.', operation: obj.operation + '0.' };
    }
  }

  console.log({
    operation: obj.operation + buttonName,
    next: null,
    symbol: buttonName,
  })
  return {
    operation: obj.operation + buttonName,
    next: null,
    symbol: buttonName,
    total: null,
  };
}
